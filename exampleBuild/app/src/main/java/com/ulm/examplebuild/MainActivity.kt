package com.ulm.examplebuild

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val ANDROID =
            android.os.Build.VERSION.RELEASE       //The current development codename, or the string "REL" if this is a release build.
        val BOARD = android.os.Build.BOARD                 //The name of the underlying board, like "goldfish".
        val BOOTLOADER = android.os.Build.BOOTLOADER            //  The system bootloader version number.
        val BRAND =
            android.os.Build.BRAND                 //The brand (e.g., carrier) the software is customized for, if any.
        val CPU_ABI =
            android.os.Build.CPU_ABI               //The name of the instruction set (CPU type + ABI convention) of native code.
        val CPU_ABI2 =
            android.os.Build.CPU_ABI2              //  The name of the second instruction set (CPU type + ABI convention) of native code.
        val DEVICE = android.os.Build.DEVICE                //  The name of the industrial design.
        val DISPLAY = android.os.Build.DISPLAY               //A build ID string meant for displaying to the user
        val FINGERPRINT = android.os.Build.FINGERPRINT           //A string that uniquely identifies this build.
        val HARDWARE =
            android.os.Build.HARDWARE              //The name of the hardware (from the kernel command line or /proc).
        val HOST = android.os.Build.HOST
        val ID = android.os.Build.ID                    //Either a changelist number, or a label like "M4-rc20".
        val MANUFACTURER = android.os.Build.MANUFACTURER          //The manufacturer of the product/hardware.
        val MODEL = android.os.Build.MODEL                 //The end-user-visible name for the end product.
        val PRODUCT = android.os.Build.PRODUCT               //The name of the overall product.
        val RADIO = android.os.Build.RADIO                 //The radio firmware version number.
        val TAGS =
            android.os.Build.TAGS                  //Comma-separated tags describing the build, like "unsigned,debug".
        val TYPE = android.os.Build.TYPE                  //The type of build, like "user" or "eng".
        val USER = android.os.Build.USER                  //
    }
}
